import { describe, expect, it, test } from 'vitest';
import { Simulator } from '../src/routes/simulator';

describe('simulator tests', () => {
	const instance = new Simulator(10, 10);

	test('nextIteration method', () => {
		it('single cell alive should die', () => {
			instance.clear();

			// setup
			instance.setAlive(0, 1, true);

			// act
			instance.nextIteration();

			// assert
			expect(instance.livings.values.length).toEqual(0);

			expect(instance.isAlive(0, 1)).toBeFalsy();
		});

		it('4 cells alive together should remain stable', () => {
			instance.clear();

			// setup
			instance.setAlive(0, 0, true);
			instance.setAlive(0, 1, true);
			instance.setAlive(1, 0, true);
			instance.setAlive(1, 1, true);

			// act
			instance.nextIteration();

			// assert
			expect(instance.isAlive(0, 0)).toBeTruthy();
			expect(instance.isAlive(1, 0)).toBeTruthy();
			expect(instance.isAlive(0, 1)).toBeTruthy();
			expect(instance.isAlive(1, 1)).toBeTruthy();
		});
	});
});
