import type { Game } from './game';

export class GameMapMaker {
	game: Game;

	constructor(_game: Game) {
		this.game = _game;
	}

	makeCthulhu() {
		const minWidth = 38;
		const minHeight = 15;
		if (this.game.nbColumns < minWidth || this.game.nbRows < minHeight) {
			alert(`need more than ${minWidth} columns and ${minHeight} rows for this pattern`);
			return;
		}
		const vShift = Math.floor((this.game.nbRows - minHeight) / 2);
		const hShift = Math.floor((this.game.nbColumns - minWidth) / 2);

		// 1st line
		this.game.setAlive(this.game.getIndexFromCoordinates(1 + vShift, 1 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 1 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 1 + hShift), true);

		// 2nd line
		this.game.setAlive(this.game.getIndexFromCoordinates(vShift, 2 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 2 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 2 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 2 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 2 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(10 + vShift, 2 + hShift), true);

		// 3rd line
		this.game.setAlive(this.game.getIndexFromCoordinates(vShift, 3 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 3 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 3 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 3 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 3 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(10 + vShift, 3 + hShift), true);

		// 4th line
		this.game.setAlive(this.game.getIndexFromCoordinates(1 + vShift, 4 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 4 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 4 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 4 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 4 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 4 + hShift), true);

		// 5th line
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 5 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 5 + hShift), true);

		// 6th line
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 6 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 6 + hShift), true);

		// 7th line
		this.game.setAlive(this.game.getIndexFromCoordinates(3 + vShift, 7 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 7 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 7 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 7 + hShift), true);

		// 8th line
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 8 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 8 + hShift), true);

		// 9th line
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 9 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(3 + vShift, 9 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 9 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 9 + hShift), true);

		// 10th line
		this.game.setAlive(this.game.getIndexFromCoordinates(3 + vShift, 10 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 10 + hShift), true);

		// 11th line
		this.game.setAlive(this.game.getIndexFromCoordinates(3 + vShift, 11 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 11 + hShift), true);

		// 12th line
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 12 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 12 + hShift), true);

		// 13th line
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 13 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 13 + hShift), true);

		// 14th line
		this.game.setAlive(this.game.getIndexFromCoordinates(1 + vShift, 14 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(2 + vShift, 14 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 14 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 14 + hShift), true);
	}

	makeChessBoard() {
		for (let i = 0; i < this.game.nbRows; i++) {
			for (let j = 0; j < this.game.nbColumns; j++) {
				if ((i + j) % 2 === 0) {
					this.game.setAlive(this.game.getIndexFromCoordinates(i, j), true);
				}
			}
		}
	}

	makeGun() {
		const minWidth = 38;
		const minHeight = 15;
		if (this.game.nbColumns < minWidth || this.game.nbRows < minHeight) {
			alert(`need more than ${minWidth} columns and ${minHeight} rows for this pattern`);
			return;
		}
		const vShift = Math.floor((this.game.nbRows - minHeight) / 2);
		const hShift = Math.floor((this.game.nbColumns - minWidth) / 2);

		// left square
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 1 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 2 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 1 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 2 + hShift), true);

		// right square
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 36 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 36 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 35 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 35 + hShift), true);

		// smiley
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 11 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 11 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 11 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(10 + vShift, 12 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 12 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 13 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 14 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(11 + vShift, 13 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(11 + vShift, 14 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 15 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 17 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 18 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 17 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 17 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 16 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(10 + vShift, 16 + hShift), true);

		// hat
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 21 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 21 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 21 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(7 + vShift, 22 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(6 + vShift, 22 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(5 + vShift, 22 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 23 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 23 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(4 + vShift, 25 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(8 + vShift, 25 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(3 + vShift, 25 + hShift), true);
		this.game.setAlive(this.game.getIndexFromCoordinates(9 + vShift, 25 + hShift), true);
	}
}
