export class Simulator {
	/**
	 * Responsible for deciding which cell is dead or alive
	 */
	livings: Map<number, Map<number, boolean>>;
	nbRows: number;
	nbColumns: number;

	constructor(nbRows: number, nbColumns: number) {
		this.nbRows = nbRows;
		this.nbColumns = nbColumns;
		this.livings = new Map<number, Map<number, boolean>>();
	}

	clear() {
		for (const row of this.livings.values()) {
			row.clear();
		}
		this.livings.clear();
	}

	isAlive(x: number, y: number): boolean {
		return this.livings.get(x)?.get(y) === true;
	}

	setAlive(x: number, y: number, bAlive: boolean) {
		if (this.livings.has(x)) {
			if (bAlive) {
				this.livings.get(x)?.set(y, bAlive);
			} else {
				this.livings.get(x)?.delete(y);
			}
			return;
		}
		if (bAlive) {
			this.livings.set(x, new Map<number, boolean>([[y, bAlive]]));
		}
	}

	nextIteration() {
		console.log('next iteration');
		return;
	}
}
