import { Simulator } from './simulator';

export class Game {
	nbRows: number;
	nbColumns: number;
	simulator: Simulator;

	/**
	 * Represents the 2d "grid" - the universe where the game of life is happening
	 */

	constructor(nbRows: number, nbColumns: number) {
		this.nbRows = nbRows;
		this.nbColumns = nbColumns;
		this.simulator = new Simulator(nbRows, nbColumns);
	}

	clear() {
		this.simulator.clear();
	}

	private getCoordinates(i: number): [number, number] {
		return [Math.floor(i / this.nbColumns), i % this.nbColumns];
	}

	getIndexFromCoordinates(x: number, y: number): number {
		return x * this.nbColumns + y;
	}

	isAlive(i: number): boolean {
		return this.simulator.isAlive(...this.getCoordinates(i));
	}

	setAlive(i: number, bAlive: boolean) {
		this.simulator.setAlive(...this.getCoordinates(i), bAlive);
	}

	step() {
		this.simulator.nextIteration();
	}
}
